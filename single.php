<?php get_header();
get_template_part("template-parts/ladate", "infolettre");

if (have_posts()) {
    while (have_posts()) {
        the_post();
        the_title();
        the_content();
    }
}

the_title("<h1>", "<h1>");
the_content();
get_footer();
