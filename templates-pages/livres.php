<?php

/**
 * Template Name: Test template livres
 */

get_header();
get_template_part("template-parts/ladate", "infolettre");
get_template_part("template-parts/ladate", "livres");

the_title("<h1>", "<h1>");
$args = array(
    'post_type' => 'livres',
    'post_per_page' => 10,
);
$loop = new WP_Query($args);

if ($loop->have_posts()) : ?>
    <?php while ($loop->have_posts()) : $loop->the_post();
        the_content(); ?>
    <?php endwhile ?>
<?php else : ?>
    <h1>pas d'articles</h1>
<?php endif;

get_footer();
