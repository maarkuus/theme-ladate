<?php

/**
 * Template Name: Test template auteur
 */

get_header();
get_template_part("template-parts/ladate", "infolettre");
get_template_part("template-parts/ladate", "profil");

the_title("<h1>", "<h1>");
if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile ?>
<?php else : ?>
    <h1>pas d'articles</h1>
<?php endif;

get_footer();
