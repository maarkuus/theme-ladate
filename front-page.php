<?php
get_header();
get_template_part("template-parts/ladate", "header");
global $variable;
$variable = "test";
get_template_part("template-parts/ladate", "profil");
get_template_part("template-parts/ladate", "livres");
get_template_part("template-parts/ladate", "critiques");
get_template_part("template-parts/ladate", "infolettre"); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <p><?php the_title() ?></p>
    <?php endwhile ?>
<?php else : ?>
    <h1>Pas d'articles</h1>
<?php endif; ?>

<?php get_footer(); ?>