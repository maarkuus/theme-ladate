   <?php $args = array(
        'post_per_page' => 3,
        'post_type' => 'ladate_livres',
    );
    $query = new WP_Query($args);
    ?>

   <section class="books">
       <div class="columns is-multiline is-mobile">

           <div class="column">
               <section class="t-1">
                   <header>
                       <h2 class="title is-4">Noublie pas - <em>La date du premier rendez-vous</em></h2>
                       <h3 class="title is-3">19 septembre 1968</h1>
                   </header>
                   <!-- <article>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam quidem nihil alias accusamus
                    repellendus! Explicabo, illum earum. Quisquam recusandae omnis quis pariatur molestiae, praesentium facere
                    vel velit saepe aliquam aperiam. -->

                   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                           <article>
                               <h2><?php the_title(); ?></h2>
                               <?php the_content() ?>
                           </article>
                       <?php endwhile;
                    else : ?>
                       <p>Aucun article(</p>
                   <?php endif; ?>
                   <!-- </article> -->
                   <footer><button class="button is-info ">Acheter</button></footer>
               </section>
           </div>
           <div class="column">
               <section class="t-2">
                   <header>
                       <h2 class="title is-4">Noublie pas - <em>La date de son anniversaire</em></h2>
                       <h3 class="title is-3">22 juillet 1948</h1>
                   </header>
                   <!-- <article>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam quidem nihil alias accusamus
                    repellendus! Explicabo, illum earum. Quisquam recusandae omnis quis pariatur molestiae, praesentium facere
                    vel velit saepe aliquam aperiam. -->

                   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                           <article>
                               <h2><?php the_title(); ?></h2>
                               <?php the_content() ?>
                           </article>
                       <?php endwhile;
                    else : ?>
                       <p>Aucun article(</p>
                   <?php endif; ?>
                   <!-- </article> -->
                   <footer><button class="button is-info ">Acheter</button></footer>
               </section>
           </div>
           <div class="column">
               <section class="t-3">
                   <header>
                       <h2 class="title is-4">Noublie pas - <em>La date du mariage</em></h2>
                       <h3 class="title is-3">19 juin 1970</h1>
                   </header>
                   <article>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam quidem nihil alias accusamus
                       repellendus! Explicabo, illum earum. Quisquam recusandae omnis quis pariatur molestiae, praesentium facere
                       vel velit saepe aliquam aperiam.</article>
                   <footer><button class="button is-info ">Acheter</button></footer>
               </section>
           </div>
       </div>
   </section>