<!-- Tout le code decette page sera exécuté au chargement du thème -->

<?php
add_theme_support('title-tag');

function enqueue_ladate_styles()
{
    wp_enqueue_style('ladate-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('principal-style', get_template_directory_uri() . '/css/main.css');
}
add_action('wp_enqueue_scripts', 'enqueue_ladate_styles');


function create_book()
{
    register_post_type(
        'livres',
        array(
            'labels'      => array(
                'name'          => __('Livres', 'textdomain'),
                'singular_name' => __('Livre', 'textdomain'),
            ),
            'public'      => true,
            'has_archive' => false,
            'supports' => [
                'excerpt', 'title', 'editor'
            ]
        )
    );
}
add_action('init', 'create_book', 10, 3);


// UTILISATION D'UN FILTRE (ajouter  un séparateur au titre du site)
function ladate_title_separator()
{
    return '|';
}
add_filter('document_title_separator', 'ladate_title_separator');

function ladate_document_title_parts($title)
{
    $title['demo'] = 'Salut';
    return $title;
}
add_filter('document_title_parts', 'ladate_document_title_parts');


function function_test()
{
    echo 1;
    echo 2;
    echo 3;
}
add_action('init', 'function_test', 10, 2);

function function_test2($a = 1, $b = 2, $c = 3)
{
    echo $a;
    echo $b;
    echo $c;
}
add_action('init', 'function_test', 20, 2);
